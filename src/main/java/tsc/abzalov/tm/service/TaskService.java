package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.model.Task;

import java.util.List;

import static org.apache.commons.lang3.StringUtils.isAnyBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static tsc.abzalov.tm.constant.LiteralConst.DEFAULT_DESCRIPTION;
import static tsc.abzalov.tm.util.InputUtil.isCorrectIndex;

public class TaskService implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public int size() {
        return taskRepository.size();
    }

    @Override
    public int indexOf(@NotNull Task task) {
        return taskRepository.indexOf(task);
    }

    @Override
    public void createTask(@NotNull String name, @NotNull String description) {
        if (isBlank(name)) return;
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        taskRepository.createTask(name, description);
    }

    @Override
    @NotNull
    public List<Task> getAllTasks() {
        return taskRepository.findAllTasks();
    }

    @Override
    @Nullable
    public Task getTaskById(@NotNull String id) {
        if (isBlank(id)) return null;
        return taskRepository.findTaskById(id);
    }

    @Override
    @Nullable
    public Task getTaskByIndex(int index) {
        final boolean isIndexCorrect = isCorrectIndex(index, taskRepository.size());
        if (isIndexCorrect) return taskRepository.findTaskByIndex(index);
        return null;
    }

    @Override
    @Nullable
    public Task getTaskByName(@NotNull String name) {
        if (isBlank(name)) return null;
        return taskRepository.findTaskByName(name);
    }

    @Override
    @Nullable
    public Task updateTaskById(@NotNull String id, @NotNull String name, @NotNull String description) {
        if (isAnyBlank(id, name)) return null;
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        return taskRepository.updateTaskById(id, name, description);
    }

    @Override
    @Nullable
    public Task updateTaskByIndex(int index, @NotNull String name, @NotNull String description) {
        final boolean isIndexCorrect = isCorrectIndex(index, taskRepository.size());
        if (!isIndexCorrect || isBlank(name)) return null;
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        return taskRepository.updateTaskByIndex(index, name, description);
    }

    @Override
    @Nullable
    public Task updateTaskByName(@NotNull String name, @NotNull String description) {
        if (isBlank(name)) return null;
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        return taskRepository.updateTaskByName(name, description);
    }

    @Override
    public void removeAllTasks() {
        taskRepository.deleteAllTasks();
    }

    @Override
    public void removeTaskById(@NotNull String id) {
        if (isBlank(id)) return;
        taskRepository.deleteTaskById(id);
    }

    @Override
    public void removeTaskByIndex(int index) {
        final boolean isIndexCorrect = isCorrectIndex(index, taskRepository.size());
        if (!isIndexCorrect) return ;
        taskRepository.deleteTaskByIndex(index);
    }

    @Override
    public void removeTaskByName(@NotNull String name) {
        if (isBlank(name)) return;
        taskRepository.deleteTaskByName(name);
    }

    @Override
    @Nullable
    public Task startTaskById(@NotNull String id) {
        if (isBlank(id)) return null;
        return taskRepository.startTaskById(id);
    }

    @Override
    @Nullable
    public Task endTaskById(@NotNull String id) {
        if (isBlank(id)) return null;
        return taskRepository.endTaskById(id);
    }
    
}
