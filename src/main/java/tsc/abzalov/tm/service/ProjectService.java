package tsc.abzalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.model.Project;

import java.util.List;

import static org.apache.commons.lang3.StringUtils.isAnyBlank;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static tsc.abzalov.tm.constant.LiteralConst.DEFAULT_DESCRIPTION;
import static tsc.abzalov.tm.util.InputUtil.isCorrectIndex;

public class ProjectService implements IProjectService {

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectService(@NotNull IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public int size() {
        return projectRepository.size();
    }

    @Override
    public int indexOf(@NotNull Project project) {
        return projectRepository.indexOf(project);
    }

    @Override
    public void createProject(@NotNull String name, @NotNull String description) {
        if (isBlank(name)) return;
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        projectRepository.createProject(name, description);
    }

    @Override
    @NotNull
    public List<Project> getAllProjects() {
        return projectRepository.findAllProjects();
    }

    @Override
    @Nullable
    public Project getProjectById(@NotNull String id) {
        if (isBlank(id)) return null;
        return projectRepository.findProjectById(id);
    }

    @Override
    @Nullable
    public Project getProjectByIndex(int index) {
        final boolean isIndexCorrect = isCorrectIndex(index, projectRepository.size());
        if (isIndexCorrect) return projectRepository.findProjectByIndex(index);
        return null;
    }

    @Override
    @Nullable
    public Project getProjectByName(@NotNull String name) {
        if (isBlank(name)) return null;
        return projectRepository.findProjectByName(name);
    }

    @Override
    @Nullable
    public Project updateProjectById(@NotNull String id, @NotNull String name, @NotNull String description) {
        if (isAnyBlank(id, name)) return null;
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        return projectRepository.updateProjectById(id, name, description);
    }

    @Override
    @Nullable
    public Project updateProjectByIndex(int index, @NotNull String name, @NotNull String description) {
        final boolean isIndexCorrect = isCorrectIndex(index, projectRepository.size());
        if (!isIndexCorrect || isBlank(name)) return null;
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        return projectRepository.updateProjectByIndex(index, name, description);
    }

    @Override
    @Nullable
    public Project updateProjectByName(@NotNull String name, @NotNull String description) {
        if (isBlank(name)) return null;
        if (isBlank(description)) description = DEFAULT_DESCRIPTION;
        return projectRepository.updateProjectByName(name, description);
    }

    @Override
    public void removeAllProjects() {
        projectRepository.deleteAllProjects();
    }

    @Override
    public void removeProjectById(@NotNull String id) {
        if (isBlank(id)) return;
        projectRepository.deleteProjectById(id);
    }

    @Override
    public void removeProjectByIndex(int index) {
        final boolean isIndexCorrect = isCorrectIndex(index, projectRepository.size());
        if (!isIndexCorrect) return ;
        projectRepository.deleteProjectByIndex(index);
    }

    @Override
    public void removeProjectByName(@NotNull String name) {
        if (isBlank(name)) return;
        projectRepository.deleteProjectByName(name);
    }

    @Override
    @Nullable
    public Project startProjectById(@NotNull String id) {
        if (isBlank(id)) return null;
        return projectRepository.startProjectById(id);
    }

    @Override
    @Nullable
    public Project endProjectById(@NotNull String id) {
        if (isBlank(id)) return null;
        return projectRepository.endProjectById(id);
    }
    
}
