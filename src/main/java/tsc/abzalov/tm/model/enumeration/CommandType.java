package tsc.abzalov.tm.model.enumeration;

import org.jetbrains.annotations.NotNull;

public enum CommandType {

    BASIC_COMMAND("Basic Command"),
    PROJECT_COMMAND("Project Command"),
    TASK_COMMAND("Task Command"),
    INTERACTION_COMMAND("Interaction Command");

    @NotNull
    private final String name;

    CommandType(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getName() {
        return this.name;
    }

}
