package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.model.enumeration.Status;

import java.util.UUID;

import static tsc.abzalov.tm.constant.LiteralConst.DEFAULT_DESCRIPTION;
import static tsc.abzalov.tm.model.enumeration.Status.TODO;

public class Task {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    @NotNull
    private String description = DEFAULT_DESCRIPTION;

    @NotNull
    private Status status = TODO;

    @Nullable
    private String projectId;

    public Task(@NotNull String name) {
        this.name = name;
    }

    public Task(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    public String getId() {
        return this.id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return this.name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull Status status) {
        this.status = status;
    }

    @Nullable
    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(@Nullable String projectId) {
        this.projectId = projectId;
    }

    @Override
    public String toString() {
        final String correctProjectId = (this.projectId == null) ? "Empty" : this.projectId;
        return this.name +
                ": [ID: " + this.id +
                "; Description: " + this.description +
                "; Status: " + this.status.getDisplayName() +
                "; Project ID: " + correctProjectId + "]";
    }

}
