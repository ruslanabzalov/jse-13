package tsc.abzalov.tm.model;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.model.enumeration.Status;

import java.util.UUID;

import static tsc.abzalov.tm.constant.LiteralConst.DEFAULT_DESCRIPTION;
import static tsc.abzalov.tm.model.enumeration.Status.TODO;

public class Project {

    @NotNull
    private String id = UUID.randomUUID().toString();

    @NotNull
    private String name;

    @NotNull
    private String description = DEFAULT_DESCRIPTION;

    @NotNull
    private Status status = TODO;

    public Project(@NotNull String name) {
        this.name = name;
    }

    public Project(@NotNull String name, @NotNull String description) {
        this.name = name;
        this.description = description;
    }

    @NotNull
    public String getId() {
        return this.id;
    }

    public void setId(@NotNull String id) {
        this.id = id;
    }

    @NotNull
    public String getName() {
        return this.name;
    }

    public void setName(@NotNull String name) {
        this.name = name;
    }

    @NotNull
    public String getDescription() {
        return description;
    }

    public void setDescription(@NotNull String description) {
        this.description = description;
    }

    @NotNull
    public Status getStatus() {
        return status;
    }

    public void setStatus(@NotNull Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return this.name +
                ": [ID: " + this.id +
                "; Description: " + this.description +
                "; Status: " + this.status.getDisplayName() + "]";
    }

}
