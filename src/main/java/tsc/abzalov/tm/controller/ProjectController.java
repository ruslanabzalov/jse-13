package tsc.abzalov.tm.controller;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.controller.IProjectController;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.model.Project;

import java.util.List;

import static tsc.abzalov.tm.util.InputUtil.*;

public class ProjectController implements IProjectController {

    @NotNull
    private final IProjectService projectService;

    public ProjectController(@NotNull IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void createProject() {
        System.out.println("PROJECT CREATION\n");
        final String projectName = inputName();
        final String projectDescription = inputDescription();
        projectService.createProject(projectName, projectDescription);

        System.out.println("Project was successfully created.\n");
    }

    @Override
    public void showAllProjects() {
        System.out.println("ALL PROJECTS LIST\n");
        if (areProjectsExist()) {
            final List<Project> projects = projectService.getAllProjects();
            projects.forEach(project -> System.out.println((projectService.indexOf(project) + 1) + ". " + project));
            System.out.println();
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

    @Override
    public void showProjectById() {
        System.out.println("FIND PROJECT BY ID\n");
        if (areProjectsExist()) {
            final String projectId = inputId();
            System.out.println();

            final Project project = projectService.getProjectById(projectId);
            if (project == null) {
                System.out.println("Searched project was not found.\n");
                return;
            }

            System.out.println((projectService.indexOf(project) + 1) + ". " + project + "\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

    @Override
    public void showProjectByIndex() {
        System.out.println("FIND PROJECT BY INDEX\n");
        if (areProjectsExist()) {
            final int projectIndex = inputIndex();
            System.out.println();

            final Project project = projectService.getProjectByIndex(projectIndex);
            if (project == null) {
                System.out.println("Searched project was not found.\n");
                return;
            }

            System.out.println((projectService.indexOf(project) + 1) + ". " + project + "\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

    @Override
    public void showProjectByName() {
        System.out.println("FIND PROJECT BY NAME\n");
        if (areProjectsExist()) {
            final String projectName = inputName();
            System.out.println();

            final Project project = projectService.getProjectByName(projectName);
            if (project == null) {
                System.out.println("Searched project was not found.\n");
                return;
            }

            System.out.println((projectService.indexOf(project) + 1) + ". " + project + "\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

    @Override
    public void editProjectById() {
        System.out.println("EDIT PROJECT BY ID\n");
        if (areProjectsExist()) {
            final String projectId = inputId();
            final String projectName = inputName();
            final String projectDescription = inputDescription();
            System.out.println();

            final Project project = projectService.updateProjectById(projectId, projectName, projectDescription);
            if (project == null) {
                System.out.println("Project was not updated! Please, check that project exists and try again.");
                return;
            }

            System.out.println("Project was successfully updated.");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

    @Override
    public void editProjectByIndex() {
        System.out.println("EDIT PROJECT BY INDEX\n");
        if (areProjectsExist()) {
            final int projectIndex = inputIndex();
            final String projectName = inputName();
            final String projectDescription = inputDescription();
            System.out.println();

            final Project project = projectService.updateProjectByIndex(projectIndex, projectName, projectDescription);
            if (project == null) {
                System.out.println("Project was not updated! Please, check that project exists and try again.");
                return;
            }

            System.out.println("Project was successfully updated.");
            return;
        }
        System.out.println("Projects list is empty.\n");
    }

    @Override
    public void editProjectByName() {
        System.out.println("EDIT PROJECT BY NAME\n");
        if (areProjectsExist()) {
            final String projectName = inputName();
            final String projectDescription = inputDescription();
            System.out.println();

            final Project project = projectService.updateProjectByName(projectName, projectDescription);
            if (project == null) {
                System.out.println("Project was not updated! Please, check that project exists and try again.");
                return;
            }

            System.out.println("Project was successfully updated.");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

    @Override
    public void deleteAllProjects() {
        System.out.println("PROJECTS DELETION\n");
        if (areProjectsExist()) {
            projectService.removeAllProjects();
            System.out.println("All projects were successfully deleted.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

    @Override
    public void deleteProjectById() {
        System.out.println("DELETE PROJECT BY ID\n");
        if (areProjectsExist()) {
            projectService.removeProjectById(inputId());
            System.out.println("Project was successfully deleted.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

    @Override
    public void deleteProjectByIndex() {
        System.out.println("DELETE PROJECT BY INDEX\n");
        if (areProjectsExist()) {
            projectService.removeProjectByIndex(inputIndex());
            System.out.println("Project was successfully deleted.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

    @Override
    public void deleteProjectsByName() {
        System.out.println("DELETE PROJECTS BY NAME\n");
        if (areProjectsExist()) {
            projectService.removeProjectByName(inputName());
            System.out.println("Projects were successfully deleted.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

    @Override
    public void startProjectById() {
        System.out.println("START PROJECT BY ID\n");
        if (areProjectsExist()) {
            final Project project = projectService.startProjectById(inputId());
            if (project == null) {
                System.out.println("Project was not started! Please, check that project exists or it has correct status.\n");
                return;
            }

            System.out.println("Project was successfully started.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

    @Override
    public void endProjectById() {
        System.out.println("END PROJECT BY ID\n");
        if (areProjectsExist()) {
            final Project project = projectService.endProjectById(inputId());
            if (project == null) {
                System.out.println("Project was not ended! Please, check that project exists or it has correct status.\n");
                return;
            }

            System.out.println("Project was successfully ended.\n");
            return;
        }

        System.out.println("Projects list is empty.\n");
    }

    private boolean areProjectsExist() {
        return projectService.size() != 0;
    }

}
