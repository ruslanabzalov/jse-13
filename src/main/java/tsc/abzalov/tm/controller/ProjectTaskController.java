package tsc.abzalov.tm.controller;

import org.jetbrains.annotations.NotNull;
import tsc.abzalov.tm.api.controller.IProjectTaskController;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.model.Task;
import tsc.abzalov.tm.util.InputUtil;

import java.util.List;

public class ProjectTaskController implements IProjectTaskController {

    @NotNull
    private final IProjectTaskService projectTasksService;

    public ProjectTaskController(@NotNull IProjectTaskService projectTasksService) {
        this.projectTasksService = projectTasksService;
    }

    @Override
    public void addTaskToProject() {
        System.out.println("ADD TASK TO PROJECT\n");
        System.out.println("Project");
        final String projectId = InputUtil.inputId();
        System.out.println();

        if (projectTasksService.findProjectById(projectId) == null) {
            System.out.println("Project was not found.\n");
            return;
        }

        System.out.println("Task");
        final String taskId = InputUtil.inputId();
        System.out.println();

        if (projectTasksService.findTaskById(taskId) == null) {
            System.out.println("Task was not found.\n");
            return;
        }

        projectTasksService.addTaskToProjectById(projectId, taskId);
        System.out.println("Task was successfully added to the project.\n");
    }

    @Override
    public void showProjectTasks() {
        System.out.println("SHOW PROJECT TASKS\n");
        if (projectTasksService.hasData()) {
            System.out.println("Project");
            String projectId = InputUtil.inputId();
            System.out.println();

            List<Task> foundedTasks = projectTasksService.findProjectTasksById(projectId);
            if (foundedTasks.size() == 0) {
                System.out.println("Tasks list is empty.\n");
                return;
            }

            foundedTasks.forEach(task -> System.out.println((projectTasksService.indexOf(task) + 1) + ". " + task));
            System.out.println();
            return;
        }

        System.out.println("Some repo is empty!");
    }

    @Override
    public void deleteProjectTask() {
        System.out.println("DELETE TASK FROM PROJECT\n");
        if (projectTasksService.hasData()) {
            System.out.println("Task");
            final String taskId = InputUtil.inputId();
            System.out.println();

            if (projectTasksService.findTaskById(taskId) == null) {
                System.out.println("Task was not found.\n");
                return;
            }

            projectTasksService.deleteProjectTaskById(taskId);
            System.out.println("Task was deleted from the project.\n");
            return;
        }

        System.out.println("Some repo is empty!");
    }

    @Override
    public void deleteProjectWithTasks() {
        System.out.println("DELETE PROJECTS WITH TASKS\n");
        if (projectTasksService.hasData()) {
            System.out.println("Project");
            String projectId = InputUtil.inputId();
            System.out.println();

            if (projectTasksService.findProjectById(projectId) == null) {
                System.out.println("Project was not found.\n");
                return;
            }

            projectTasksService.deleteProjectTasksById(projectId);
            projectTasksService.deleteProjectById(projectId);
            System.out.println("Project and it tasks was successfully deleted.\n");
            return;
        }

        System.out.println("Some repo is empty!");
    }

}
