package tsc.abzalov.tm.bootstrap;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import tsc.abzalov.tm.api.controller.ICommandController;
import tsc.abzalov.tm.api.controller.IProjectController;
import tsc.abzalov.tm.api.controller.IProjectTaskController;
import tsc.abzalov.tm.api.controller.ITaskController;
import tsc.abzalov.tm.api.repository.ICommandRepository;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.api.service.ICommandService;
import tsc.abzalov.tm.api.service.IProjectService;
import tsc.abzalov.tm.api.service.IProjectTaskService;
import tsc.abzalov.tm.api.service.ITaskService;
import tsc.abzalov.tm.controller.CommandController;
import tsc.abzalov.tm.controller.ProjectController;
import tsc.abzalov.tm.controller.ProjectTaskController;
import tsc.abzalov.tm.controller.TaskController;
import tsc.abzalov.tm.repository.CommandRepository;
import tsc.abzalov.tm.repository.ProjectRepository;
import tsc.abzalov.tm.repository.TaskRepository;
import tsc.abzalov.tm.service.CommandService;
import tsc.abzalov.tm.service.ProjectService;
import tsc.abzalov.tm.service.ProjectTaskService;
import tsc.abzalov.tm.service.TaskService;

import static tsc.abzalov.tm.constant.ApplicationArgumentConst.*;
import static tsc.abzalov.tm.constant.ApplicationCommandConst.*;
import static tsc.abzalov.tm.util.InputUtil.INPUT;

public class CommandBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final IProjectTaskService projectTasksService = new ProjectTaskService(projectRepository, taskRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectController projectController = new ProjectController(projectService);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskController projectTasksController = new ProjectTaskController(projectTasksService);

    public void run(String... args) {
        System.out.println();
        if (areArgExists(args)) return;

        System.out.println("****** WELCOME TO TASK MANAGER APPLICATION ******");
        System.out.println("Please, use \"help\" command to see all available commands.\n");

        String command;
        //noinspection InfiniteLoopStatement
        while (true) {
            System.out.print("Please, enter your command: ");
            command = INPUT.nextLine();
            System.out.println();
            if (StringUtils.isEmpty(command)) continue;
            processCommand(command);
        }
    }

    private boolean areArgExists(String... args) {
        if (ArrayUtils.isEmpty(args)) return false;

        final String arg = args[0];
        if (StringUtils.isEmpty(arg)) return false;

        processArg(arg);
        return true;
    }

    private void processArg(String arg) {
        switch (arg) {
            case ARG_HELP:
                commandController.showHelp();
                break;
            case ARG_INFO:
                commandController.showInfo();
                break;
            case ARG_ABOUT:
                commandController.showAbout();
                break;
            case ARG_VERSION:
                commandController.showVersion();
                break;
            case ARG_COMMANDS:
                commandController.showCommandNames();
                break;
            case ARG_ARGUMENTS:
                commandController.showCommandArgs();
                break;
            default:
                commandController.showError(arg, true);
        }
    }

    private void processCommand(String command) {
        switch (command) {
            case CMD_HELP:
                commandController.showHelp();
                break;
            case CMD_INFO:
                commandController.showInfo();
                break;
            case CMD_ABOUT:
                commandController.showAbout();
                break;
            case CMD_VERSION:
                commandController.showVersion();
                break;
            case CMD_COMMANDS:
                commandController.showCommandNames();
                break;
            case CMD_ARGUMENTS:
                commandController.showCommandArgs();
                break;
            case CMD_EXIT:
                commandController.exit();
            case CMD_CREATE_PROJECT:
                projectController.createProject();
                break;
            case CMD_SHOW_ALL_PROJECTS:
                projectController.showAllProjects();
                break;
            case CMD_SHOW_PROJECT_BY_ID:
                projectController.showProjectById();
                break;
            case CMD_SHOW_PROJECT_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CMD_SHOW_PROJECT_BY_NAME:
                projectController.showProjectByName();
                break;
            case CMD_UPDATE_PROJECT_BY_ID:
                projectController.editProjectById();
                break;
            case CMD_UPDATE_PROJECT_BY_INDEX:
                projectController.editProjectByIndex();
                break;
            case CMD_UPDATE_PROJECT_BY_NAME:
                projectController.editProjectByName();
                break;
            case CMD_DELETE_ALL_PROJECTS:
                projectController.deleteAllProjects();
                break;
            case CMD_DELETE_PROJECT_BY_ID:
                projectController.deleteProjectById();
                break;
            case CMD_DELETE_PROJECT_BY_INDEX:
                projectController.deleteProjectByIndex();
                break;
            case CMD_DELETE_PROJECTS_BY_NAME:
                projectController.deleteProjectsByName();
                break;
            case CMD_START_PROJECT:
                projectController.startProjectById();
                break;
            case CMD_END_PROJECT:
                projectController.endProjectById();
                break;
            case CMD_CREATE_TASK:
                taskController.createTask();
                break;
            case CMD_SHOW_ALL_TASKS:
                taskController.showAllTasks();
                break;
            case CMD_SHOW_TASK_BY_ID:
                taskController.showTaskById();
                break;
            case CMD_SHOW_TASK_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CMD_SHOW_TASK_BY_NAME:
                taskController.showTaskByName();
                break;
            case CMD_UPDATE_TASK_BY_ID:
                taskController.editTaskById();
                break;
            case CMD_UPDATE_TASK_BY_INDEX:
                taskController.editTaskByIndex();
                break;
            case CMD_UPDATE_TASK_BY_NAME:
                taskController.editTaskByName();
                break;
            case CMD_DELETE_ALL_TASKS:
                taskController.deleteAllTasks();
                break;
            case CMD_DELETE_TASK_BY_ID:
                taskController.deleteTaskById();
                break;
            case CMD_DELETE_TASK_BY_INDEX:
                taskController.deleteTaskByIndex();
                break;
            case CMD_DELETE_TASKS_BY_NAME:
                taskController.deleteTasksByName();
                break;
            case CMD_START_TASK:
                taskController.startTaskById();
                break;
            case CMD_END_TASK:
                taskController.endTaskById();
                break;
            case CMD_ADD_TASK_TO_PROJECT:
                projectTasksController.addTaskToProject();
                break;
            case CMD_DELETE_PROJECT_TASK:
                projectTasksController.deleteProjectTask();
                break;
            case CMD_SHOW_PROJECT_TASKS:
                projectTasksController.showProjectTasks();
                break;
            case CMD_DELETE_PROJECT_WITH_TASKS:
                projectTasksController.deleteProjectWithTasks();
                break;
            default:
                commandController.showError(command, false);
        }
    }

}
