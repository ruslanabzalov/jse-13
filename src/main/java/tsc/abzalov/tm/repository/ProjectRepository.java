package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.IProjectRepository;
import tsc.abzalov.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

import static tsc.abzalov.tm.model.enumeration.Status.*;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public int size() {
        return projects.size();
    }

    @Override
    public int indexOf(@NotNull Project project) {
        return projects.indexOf(project);
    }

    @Override
    public void createProject(@NotNull String id, @NotNull String description) {
        projects.add(new Project(id, description));
    }

    @Override
    @NotNull
    public List<Project> findAllProjects() {
        return projects;
    }

    @Override
    @Nullable
    public Project findProjectById(@NotNull String id) {
        for (final Project project : projects)
            if (project.getId().equals(id)) return project;
        return null;
    }

    @Override
    @Nullable
    public Project findProjectByIndex(int index) {
        return projects.get(index);
    }

    @Override
    @Nullable
    public Project findProjectByName(@NotNull String name) {
        for (final Project project : projects)
            if (project.getName().equals(name)) return project;
        return null;
    }

    @Override
    @Nullable
    public Project updateProjectById(@NotNull String id, @NotNull String name, @NotNull String description) {
        final Project project = findProjectById(id);
        if (project == null) return null;

        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    @Nullable
    public Project updateProjectByIndex(int index, @NotNull String name, @NotNull String description) {
        final Project project = findProjectByIndex(index);
        if (project == null) return null;

        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    @Nullable
    public Project updateProjectByName(@NotNull String name, @NotNull String description) {
        final Project project = findProjectByName(name);
        if (project == null) return null;

        project.setDescription(description);
        return project;
    }

    @Override
    public void deleteAllProjects() {
        projects.clear();
    }

    @Override
    public void deleteProjectById(@NotNull String id) {
        final Project project = findProjectById(id);
        if (project == null) return;

        projects.remove(project);
    }

    @Override
    public void deleteProjectByIndex(int index) {
        final Project project = projects.get(index);
        if (project == null) return;

        projects.remove(project);
    }

    @Override
    public void deleteProjectByName(@NotNull String name) {
        final Project project = findProjectByName(name);
        if (project == null) return;

        projects.remove(project);
    }

    @Override
    public Project startProjectById(@NotNull String id) {
        final Project project = findProjectById(id);
        if (project == null || project.getStatus() != TODO) return null;

        project.setStatus(IN_PROGRESS);
        return project;
    }

    @Override
    public Project endProjectById(@NotNull String id) {
        final Project project = findProjectById(id);
        if (project == null || project.getStatus() != IN_PROGRESS) return null;

        project.setStatus(DONE);
        return project;
    }

}
