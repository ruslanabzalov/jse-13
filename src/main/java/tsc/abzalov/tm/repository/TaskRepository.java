package tsc.abzalov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import tsc.abzalov.tm.api.repository.ITaskRepository;
import tsc.abzalov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static tsc.abzalov.tm.model.enumeration.Status.*;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public int size() {
        return tasks.size();
    }

    @Override
    public int indexOf(@NotNull Task task) {
        return tasks.indexOf(task);
    }

    @Override
    public void createTask(@NotNull String id, @NotNull String description) {
        tasks.add(new Task(id, description));
    }

    @Override
    public void addTaskToProjectById(@NotNull String projectId, @NotNull String taskId) {
        for (final Task task : tasks) {
            if (task.getId().equals(taskId)) {
                task.setProjectId(projectId);
                return;
            }
        }
    }

    @Override
    @NotNull
    public List<Task> findAllTasks() {
        return tasks;
    }

    @Override
    @Nullable
    public Task findTaskById(@NotNull String id) {
        for (final Task task : tasks)
            if (task.getId().equals(id)) return task;
        return null;
    }

    @Override
    @Nullable
    public Task findTaskByIndex(int index) {
        return tasks.get(index);
    }

    @Override
    @Nullable
    public Task findTaskByName(@NotNull String name) {
        for (final Task task : tasks)
            if (task.getName().equals(name)) return task;
        return null;
    }

    @Override
    @NotNull
    public List<Task> findProjectTasksById(@NotNull String projectId) {
        return tasks.stream()
                .filter(task -> isProjectTaskExist(projectId, task))
                .collect(Collectors.toList());
    }


    @Override
    @Nullable
    public Task updateTaskById(@NotNull String id, @NotNull String name, @NotNull String description) {
        final Task task = findTaskById(id);
        if (task == null) return null;

        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @Nullable
    public Task updateTaskByIndex(int index, @NotNull String name, @NotNull String description) {
        final Task task = findTaskByIndex(index);
        if (task == null) return null;

        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @Override
    @Nullable
    public Task updateTaskByName(@NotNull String name, @NotNull String description) {
        final Task task = findTaskByName(name);
        if (task == null) return null;

        task.setDescription(description);
        return task;
    }

    @Override
    public void deleteAllTasks() {
        tasks.clear();
    }

    @Override
    public void deleteTaskById(@NotNull String id) {
        final Task task = findTaskById(id);
        if (task == null) return;

        tasks.remove(task);
    }

    @Override
    public void deleteTaskByIndex(int index) {
        final Task task = tasks.get(index);
        if (task == null) return;

        tasks.remove(task);
    }

    @Override
    public void deleteTaskByName(@NotNull String name) {
        final Task task = findTaskByName(name);
        if (task == null) return;

        tasks.remove(task);
    }

    @Override
    public void deleteProjectTasksById(@NotNull String projectId) {
        tasks.removeAll(findProjectTasksById(projectId));
    }

    @Override
    public void deleteProjectTaskById(@NotNull String taskId) {
        for (final Task task : tasks) {
            if (task.getId().equals(taskId)) {
                if (task.getProjectId() != null) {
                    task.setProjectId(null);
                }
            }
        }
    }

    @Override
    public Task startTaskById(@NotNull String id) {
        final Task task = findTaskById(id);
        if (task == null || task.getStatus() != TODO) return null;

        task.setStatus(IN_PROGRESS);
        return task;
    }

    @Override
    public Task endTaskById(@NotNull String id) {
        final Task task = findTaskById(id);
        if (task == null || task.getStatus() != IN_PROGRESS) return null;

        task.setStatus(DONE);
        return task;
    }

    private boolean isProjectTaskExist(@NotNull String taskId, @NotNull Task task) {
        final String projectTaskId = task.getProjectId();
        if (projectTaskId == null) return false;
        return projectTaskId.equals(taskId);
    }
    
}
