package tsc.abzalov.tm.constant;

public final class LiteralConst {

    public static final String DEFAULT_DESCRIPTION = "Description is empty";

    private LiteralConst() {
    }

}
