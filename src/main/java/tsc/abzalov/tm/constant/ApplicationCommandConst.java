package tsc.abzalov.tm.constant;

public final class ApplicationCommandConst {

    public static final String CMD_HELP = "help";

    public static final String CMD_INFO = "info";

    public static final String CMD_ABOUT = "about";

    public static final String CMD_VERSION = "version";

    public static final String CMD_COMMANDS = "commands";

    public static final String CMD_ARGUMENTS = "arguments";

    public static final String CMD_EXIT = "exit";

    public static final String CMD_CREATE_PROJECT = "create-project";

    public static final String CMD_SHOW_ALL_PROJECTS = "show-all-projects";

    public static final String CMD_SHOW_PROJECT_BY_ID = "show-project-by-id";

    public static final String CMD_SHOW_PROJECT_BY_INDEX = "show-project-by-index";

    public static final String CMD_SHOW_PROJECT_BY_NAME = "show-project-by-name";

    public static final String CMD_UPDATE_PROJECT_BY_ID = "update-project-by-id";

    public static final String CMD_UPDATE_PROJECT_BY_INDEX = "update-project-by-index";

    public static final String CMD_UPDATE_PROJECT_BY_NAME = "update-project-by-name";

    public static final String CMD_DELETE_ALL_PROJECTS = "delete-all-projects";

    public static final String CMD_DELETE_PROJECT_BY_ID = "delete-project-by-id";

    public static final String CMD_DELETE_PROJECT_BY_INDEX = "delete-project-by-index";

    public static final String CMD_DELETE_PROJECTS_BY_NAME = "delete-projects-by-name";

    public static final String CMD_START_PROJECT = "start-project";

    public static final String CMD_END_PROJECT = "end-project";

    public static final String CMD_CREATE_TASK = "create-task";

    public static final String CMD_SHOW_ALL_TASKS = "show-all-tasks";

    public static final String CMD_SHOW_TASK_BY_ID = "show-task-by-id";

    public static final String CMD_SHOW_TASK_BY_INDEX = "show-task-by-index";

    public static final String CMD_SHOW_TASK_BY_NAME = "show-task-by-name";

    public static final String CMD_UPDATE_TASK_BY_ID = "update-task-by-id";

    public static final String CMD_UPDATE_TASK_BY_INDEX = "update-task-by-index";

    public static final String CMD_UPDATE_TASK_BY_NAME = "update-task-by-name";

    public static final String CMD_DELETE_ALL_TASKS = "delete-all-tasks";

    public static final String CMD_DELETE_TASK_BY_ID = "delete-task-by-id";

    public static final String CMD_DELETE_TASK_BY_INDEX = "delete-task-by-index";

    public static final String CMD_DELETE_TASKS_BY_NAME = "delete-tasks-by-name";

    public static final String CMD_START_TASK = "start-task";

    public static final String CMD_END_TASK = "end-task";

    public static final String CMD_ADD_TASK_TO_PROJECT = "add-task-to-project";

    public static final String CMD_DELETE_PROJECT_TASK = "delete-project-task";

    public static final String CMD_SHOW_PROJECT_TASKS = "show-project-tasks";

    public static final String CMD_DELETE_PROJECT_WITH_TASKS = "delete-project-with-tasks";

    private ApplicationCommandConst() {
    }

}