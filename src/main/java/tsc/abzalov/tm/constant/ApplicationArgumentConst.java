package tsc.abzalov.tm.constant;

public final class ApplicationArgumentConst {

    public static final String ARG_HELP = "-h";

    public static final String ARG_INFO = "-i";

    public static final String ARG_ABOUT = "-a";

    public static final String ARG_VERSION = "-v";

    public static final String ARG_COMMANDS = "--commands";

    public static final String ARG_ARGUMENTS = "--arguments";

    private ApplicationArgumentConst() {
    }

}
