package tsc.abzalov.tm.api.controller;

public interface ITaskController {

    void createTask();

    void showAllTasks();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByName();

    void editTaskById();

    void editTaskByIndex();

    void editTaskByName();

    void deleteAllTasks();

    void deleteTaskById();

    void deleteTaskByIndex();

    void deleteTasksByName();

    void startTaskById();

    void endTaskById();
    
}
