package tsc.abzalov.tm.api.controller;

public interface IProjectController {

    void createProject();

    void showAllProjects();

    void showProjectById();

    void showProjectByIndex();

    void showProjectByName();

    void editProjectById();

    void editProjectByIndex();

    void editProjectByName();

    void deleteAllProjects();

    void deleteProjectById();

    void deleteProjectByIndex();

    void deleteProjectsByName();

    void startProjectById();

    void endProjectById();

}
