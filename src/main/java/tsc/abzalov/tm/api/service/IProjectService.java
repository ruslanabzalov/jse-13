package tsc.abzalov.tm.api.service;

import tsc.abzalov.tm.model.Project;

import java.util.List;

public interface IProjectService {

    int size();

    int indexOf(Project project);

    void createProject(String name, String description);

    List<Project> getAllProjects();

    Project getProjectById(String id);

    Project getProjectByIndex(int index);

    Project getProjectByName(String name);

    Project updateProjectById(String id, String name, String description);

    Project updateProjectByIndex(int index, String name, String description);

    Project updateProjectByName(String name, String description);

    void removeAllProjects();

    void removeProjectById(String id);

    void removeProjectByIndex(int index);

    void removeProjectByName(String name);

    Project startProjectById(String id);

    Project endProjectById(String id);
    
}
